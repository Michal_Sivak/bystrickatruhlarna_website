import json

from flask import render_template, redirect, url_for, flash, request, g, session

from app import db
from app.main import bp
from app.main.email import send_contact_email
from app.models import Orders, Item, Offers, Reviews


@bp.route('/', methods=['GET'])
@bp.route('/home', methods=['GET'])
def main_page():
    reviews_list = Reviews.query.all()
    offers_list = Offers.query.all()
    offers_list = [offer for offer in offers_list if not offer.archived]
    enable = False if not offers_list else True
    recent_offer = None if not offers_list else offers_list[0]
    return render_template('website/home.html', offer_enabled=enable, recent_offer=recent_offer, reviews=reviews_list)


@bp.route('/about', methods=['GET'])
def about():
    return render_template('website/about.html')


@bp.route('/contact', methods=['GET', 'POST'])
def contact():
    if request.method == "POST":
        json_data = request.values["form-data"]
        send_contact_email(json_data)
        flash("Zpráva odeslána", "success")
        return redirect(url_for("contact"))
    return render_template('website/contact.html')


@bp.route('/gallery', methods=['GET'])
def gallery():
    return render_template('website/gallery.html')


@bp.route('/services', methods=['GET'])
def services():
    return render_template('website/services.html')


@bp.route('/order', methods=['GET', 'POST'])
def order():
    def parse_data(data):
        def parse_item(item):
            return {
                "Material": item["material"] if item["material"] != "" else None,
                "Length": item["length"] if item["length"] != "" else None,
                "AbsoluteLength": item["abs_length"] if item["abs_length"] != "" else None,
                "Width": item["width"] if item["width"] != "" else None,
                "AbsoluteWidth": item["abs_width"] if item["abs_width"] != "" else None,
                "Count": item["count"] if item["count"] != "" else None,
                "Type": item["type"] if item["type"] != "" else None
            }

        return {
            "Recipient": data["recipient"] if data["recipient"] != "" else None,
            "Firm": data["firm"] if data["firm"] != "" else None,
            "Address": data["address"] if data["address"] != "" else None,
            "Phone": data["phone"] if data["phone"] != "" else None,
            "OrderName": data["order_name"] if data["order_name"] != "" else None,
            "Items": [parse_item(item) for item in data["items"]]
        }

    if request.method == "POST":
        json_data = json.loads(request.values["form-data"])
        try:
            data = parse_data(json_data)
            order = Orders(data["Recipient"], data["Firm"], data["Address"], data["Phone"], data["OrderName"])
            items = [
                Item(item_data["Material"], item_data["Length"], item_data["AbsoluteLength"],
                     item_data["Width"], item_data["AbsoluteWidth"], item_data["Count"], item_data["Type"])
                for item_data in data["Items"]
            ]
            order.items.extend(items)
            db.session.add(order)
            db.session.add_all(items)
            db.session.commit()
            flash("Objednávka odeslána", "success")
        except:
            flash("Nebylo možné objednávku odeslat, omlouváme se. Zkuste překontrolovat zadané informace", "error")
            return render_template('website/order.html', data=parse_data(json.loads(request.values["form-data"])), show=1)
        return redirect(url_for("main.order"))
    return render_template('website/order.html')


@bp.route('/order-rules')
def order_rules():
    return render_template('website/order-rules.html')


@bp.route('/offers', methods=['GET'])
def offers():
    offers_list = Offers.query.all()
    offers_list = [offer for offer in offers_list if not offer.archived]
    return render_template('website/offers.html', offers=offers_list)


@bp.route('/language/<language>')
def lang(language=None):
    session["language"] = language
    return redirect(url_for("main.main_page"))

from flask import render_template, current_app

from app.email import send_email


def send_contact_email(client_data):
    send_email('[BystřickáTruhlárna] Zpráva od zákazníka',
               sender=current_app.config['ADMINS'][0],
               recipients=[client_data["email"]],
               text_body=render_template('email/message_from_client.txt', client_data=client_data),
               html_body=render_template('email/message_from_client.html', client_data=client_data))

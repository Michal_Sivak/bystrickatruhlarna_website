import math
from datetime import datetime
import html

from flask import render_template, redirect, url_for, request, session, json
from flask_login import login_user, logout_user, current_user, login_required

from app import db
from app.admin import bp
from app.admin.forms import LoginForm
from app.localization import init, read_languages_from_db, update, compile, save_keys_to_db
from app.models import User, Orders, Offers, Reviews, Languages, Item


def set_sessions():
    session["ShowArchivedOrders"] = False
    session["ShowArchivedOffers"] = False
    session["PerPage"] = 20


@bp.route('/', methods=['GET'])
@bp.route('/home', methods=['GET'])
def index():
    if current_user.is_authenticated:
        return render_template('admin/index.html')
    return redirect(url_for('admin.login'))


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('admin.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            return redirect(url_for('admin.login'))
        login_user(user, remember=form.remember_me.data)
        set_sessions()
        return redirect(url_for('admin.index'))
    return render_template('admin/login.html', form=form)


@bp.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('admin.login'))


@bp.route('/password-change', methods=['GET', 'POST'])
@login_required
def password_change():
    if request.method == "POST":
        json_data = json.loads(request.values["form-data"])
        user = User.query.filter_by(username=json_data["username"]).first()
        if user is not None and user.check_password(json_data["old_password"]):
            user.set_password(json_data["new_password"])
            db.session.commit()
            return redirect(url_for('admin.logout'))
        return redirect(url_for('admin.password_change'))
    return render_template('admin/password_change.html', user=current_user)


@bp.route('/orders', methods=['GET'])
@bp.route('/orders/<int:page>', methods=['GET'])
@login_required
def orders(page=1):
    orders_list = Orders.query.all()
    if len(orders_list) == 0:
        return render_template("admin/orders.html", archived=session["ShowArchivedOrders"])
    if not session["ShowArchivedOrders"]:
        orders_list = [order for order in orders_list if not order.archived]
    last = math.ceil(len(orders_list) / session["PerPage"])
    if page > last or page < 1:
        return render_template('errors/404.html'), 404
    orders_list = orders_list[(page - 1) * session["PerPage"]:page * session["PerPage"]]
    orders_list.sort(key=lambda x: bool(x.archived))
    pagination_data = {
        "next_exists": True if page != last else False,
        "previous_exists": True if page != 1 else False,
        "previous": (page - 1) if page != 1 else 1,
        "next": (page + 1) if page != last else last,
        "last": last,
        "current": page
    }
    return render_template('admin/orders.html', orders=orders_list, page=pagination_data,
                           archived=session["ShowArchivedOrders"])


@bp.route('/orders/archive', methods=['GET', 'POST'])
@login_required
def orders_archive():
    if request.method == "POST":
        order_id = request.values["id"]
        order = Orders.query.filter_by(id=order_id).first()
        order.archived = not order.archived
        db.session.commit()
        return redirect(url_for('admin.orders'))
    session["ShowArchivedOrders"] = not session["ShowArchivedOrders"]
    return redirect(url_for('admin.orders'))


@bp.route('/orders/delete', methods=['POST'])
@login_required
def orders_delete():
    order_id = request.values["id"]
    order = Orders.query.filter_by(id=order_id).first()
    order_items = Item.query.filter_by(item_id=order_id).all()
    for order_item in order_items:
        db.session.delete(order_item)
    db.session.delete(order)
    db.session.commit()
    return redirect(url_for('admin.orders'))


@bp.route('/offers', methods=['GET'])
@bp.route('/offers/<int:page>', methods=['GET'])
@login_required
def offers(page=1):
    offers_list = Offers.query.all()
    if len(offers_list) == 0:
        return render_template("admin/offers.html")
    last = math.ceil(len(offers_list) / session["PerPage"])
    if page > last or page < 1:
        return render_template('errors/404.html'), 404
    offers_list = offers_list[(page - 1) * session["PerPage"]:page * session["PerPage"]]
    offers_list.sort(key=lambda x: bool(x.archived))
    pagination_data = {
        "next_exists": True if page != last else False,
        "previous_exists": True if page != 1 else False,
        "previous": (page - 1) if page != 1 else 1,
        "next": (page + 1) if page != last else last,
        "last": last,
        "current": page
    }
    return render_template("admin/offers.html", offers=offers_list, page=pagination_data)


@bp.route('/offers/add', methods=['GET', 'POST'])
@login_required
def offers_add():
    def get_datetime(string):
        return datetime.strptime(string, "%d/%m/%Y")

    if request.method == "POST":
        json_data = json.loads(request.values["form-data"])
        offer = Offers(json_data["name"], get_datetime(json_data["from-date"]), get_datetime(json_data["to-date"]),
                       json_data["offer-text"])
        db.session.add(offer)
        db.session.commit()
        return redirect(url_for('admin.offers_add'))
    return render_template("admin/add_offer.html")


@bp.route('/offers/delete', methods=['POST'])
@login_required
def offers_delete():
    offer_id = request.values["id"]
    offer = Offers.query.filter_by(id=offer_id).first()
    db.session.delete(offer)
    db.session.commit()
    return redirect(url_for('admin.offers'))


@bp.route('/reviews', defaults={"page": 1}, methods=['GET'])
@bp.route('/reviews/<int:page>', methods=['GET'])
@login_required
def reviews(page):
    reviews_list = Reviews.query.all()
    if len(reviews_list) == 0:
        return render_template("admin/reviews.html")
    last = math.ceil(len(reviews_list) / session["PerPage"])
    if page > last or page < 1:
        return render_template('errors/404.html'), 404
    reviews_list = reviews_list[(page - 1) * session["PerPage"]:page * session["PerPage"]]
    pagination_data = {
        "next_exists": True if page != last else False,
        "previous_exists": True if page != 1 else False,
        "previous": (page - 1) if page != 1 else 1,
        "next": (page + 1) if page != last else last,
        "last": last,
        "current": page
    }
    return render_template('admin/reviews.html', reviews=reviews_list, page=pagination_data)


@bp.route('/reviews/add', methods=['GET', 'POST'])
@login_required
def reviews_add():
    if request.method == "POST":
        json_data = json.loads(request.values["form-data"])
        review = Reviews(json_data["firm_name"], json_data["person_name"], json_data["text"])
        db.session.add(review)
        db.session.commit()
        return redirect(url_for('admin.reviews_add'))
    return render_template("admin/add_review.html")


@bp.route('/reviews/delete', methods=['POST'])
@login_required
def reviews_delete():
    review_id = request.values["id"]
    review = Reviews.query.filter_by(id=review_id).first()
    db.session.delete(review)
    db.session.commit()
    return redirect(url_for('admin.reviews'))


@bp.route('/localization', methods=['GET'])
@bp.route('/localization/<int:page>', methods=['GET'])
@login_required
def localization(page=1):
    text = request.args.get("search") if request.args.get("search") is not None else ""
    if text != "":
        languages_list = Languages.query.filter(Languages.czech.like("%"+text+"%")).all()
        languages_list += Languages.query.filter(Languages.english.like("%"+text+"%")).all()
        languages_list += Languages.query.filter(Languages.german.like("%"+text+"%")).all()
        if len(languages_list) == 0:
            return render_template("admin/localization.html")
        pagination_data = {
            "next_exists": False,
            "previous_exists": False,
            "previous": 1,
            "next": 1,
            "last": 1,
            "current": 1
        }
    else:
        languages_list = Languages.query.all()
        if len(languages_list) == 0:
            return render_template("admin/localization.html")
        last = math.ceil(len(languages_list) / session["PerPage"])
        if page > last or page < 1:
            return render_template('errors/404.html'), 404
        languages_list = languages_list[(page - 1) * session["PerPage"]:page * session["PerPage"]]
        pagination_data = {
            "next_exists": True if page != last else False,
            "previous_exists": True if page != 1 else False,
            "previous": (page - 1) if page != 1 else 1,
            "next": (page + 1) if page != last else last,
            "last": last,
            "current": page
        }
    return render_template('admin/localization.html', languages=languages_list, page=pagination_data)


@bp.route('/localization/edit/<int:sentence_id>', methods=['GET', 'POST'])
@login_required
def localization_edit(sentence_id=1):
    if request.method == "POST":
        json_data = request.values
        sentence = Languages.query.filter_by(id=sentence_id).first()
        sentence.czech = json_data["czech"] if json_data["czech"] != "" else None
        sentence.english = json_data["english"] if json_data["english"] != "" else None
        sentence.german = json_data["german"] if json_data["german"] != "" else None
        db.session.commit()
        return redirect(url_for('admin.localization_edit', sentence_id=sentence_id))
    sentence = Languages.query.filter_by(id=sentence_id).first()
    sentences_ids = [sentence.id for sentence in Languages.query.all()]
    sentences_ids.sort()
    index_of_sentence = sentences_ids.index(sentence.id) + 1
    pagination_data = {
        "previous": (index_of_sentence - 1) if index_of_sentence != 1 else 1,
        "next": (index_of_sentence + 1) if index_of_sentence < len(sentences_ids) else len(sentences_ids),
        "current": index_of_sentence
    }
    return render_template("admin/edit_localization.html", sentence=sentence, page=pagination_data)


@bp.route('/localization/clear', methods=['GET'])
@login_required
def localization_clear():
    db.session.query(Languages).delete()
    db.session.commit()
    return redirect(url_for('admin.localization'))


@bp.route('/localization/load', methods=['GET'])
@login_required
def localization_load():
    init("cs")
    init("en")
    init("de")
    save_keys_to_db()
    return redirect(url_for('admin.localization'))


@bp.route('/localization/reload', methods=['GET'])
@login_required
def localization_reload():
    read_languages_from_db("cs")
    read_languages_from_db("en")
    read_languages_from_db("de")
    update()
    compile()
    return redirect(url_for('admin.localization'))

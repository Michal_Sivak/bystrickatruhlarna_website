function include(url){ 
  document.write('<script src="../static/js/'+ url + '"></script>'); 
  return false ;
}
var currentYear = (new Date).getFullYear();
$(document).ready(function() {
$("#copyright-year").text( (new Date).getFullYear() );
});
include('device.min.js');
include('tmstickup.js');
$(window).ready(function() { 
  if ($('html').hasClass('desktop')) {
      $('#stuck_container').TMStickUp({
      })
  }  
});
include('superfish.js');
// include('jquery.mousewheel.min.js');
// include('jquery.simplr.smoothscroll.min.js');
// var macOrpc = navigator.userAgent.indexOf('Mac OS X');
// $(function () { 
//   if (($('html').hasClass('desktop')) && (macOrpc==-1)) {
//       $.srSmoothscroll({
//         step:150,
//         speed:800
//       });
	  
//   }   
// });
include('jquery.rd-parallax.js');
include('jquery.ui.totop.js');
$(function () {   
  $().UItoTop({ easingType: 'easeOutQuart' });
  $('.block-1 a').on('click', function(e){
    e.preventDefault();
  });
  $('.offer-close-btn').click(function(e) {
    e.preventDefault();
    $('.overlay').css({display: "none", opacity: 0});
  })  
  if(!!document.querySelector('.overlay'))
  {
    setTimeout(function(){
      $('.overlay').css({display: "flex"}).animate({
        opacity: 1
      }, 1250)
    }, 5000)
  }
  if(!!document.querySelector('.owl-carousel'))
  {
    var owl = jQuery('.owl-carousel');
			owl.owlCarousel({
				items: 1,
				autoplay: true,
				autoplayTimeout: 5000,
				autoplaySpeed: 1000,
				nav: false,
				startPosition: 2,
				768 : {
					margin: 180,
					stagePadding: 55,
				},
				0 : {
					margin: 55,
					stagePadding: 55,
				},
    });
  }
});
jQuery(function(){
      jQuery('.sf-menu').mobileMenu();
    })
$(function(){
  var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
    ua = navigator.userAgent,
 
    gestureStart = function () {
        viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0";
    },
 
    scaleFix = function () {
      if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
        viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
        document.addEventListener("gesturestart", gestureStart, false);
      }
    };
scaleFix();

if(window.orientation!=undefined){
 var regM = /ipod|ipad|iphone/gi,
  result = ua.match(regM)
 if(!result) {
  $('.sf-menu li').each(function(){

   if($(">ul", this)[0]){
    $(">a", this).toggle(
     function(){
      return false;
     },
     function(){
      window.location.href = $(this).attr("href");
     }
    );
   } 
  })
 }
}
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
};
//document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">');



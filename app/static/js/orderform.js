function parseForm() {
    var recipient = document.querySelector('.recipient').value;
    var firm = document.querySelector('.firm').value;
    var address = document.querySelector('.address').value;
    var phone = document.querySelector('.phone').value;
    var order_name = document.querySelector('.order-name').value;

    var items = Array.prototype.slice.call(document.querySelectorAll(".item-row:not(#item-row-template)")).map(function(x) {
        return {
            "material": x.querySelector(".material").value,
            "length": x.querySelector(".length").value,
            "abs_length": x.querySelector(".absLength").value,
            "width": x.querySelector(".width").value,
            "abs_width": x.querySelector(".absWidth").value,
            "count": x.querySelector(".count").value,
            "type": x.querySelector(".type").value
        } 
    });

    var response = {
        "recipient" : recipient,
        "firm" : firm,
        "address" : address,
        "phone" : phone,
        "order_name" : order_name,
        "items" : items
    };

    var responseJSON = JSON.stringify(response);
    $("#formData").val(responseJSON);
}

$(function(){
    function cloneRow() {
        var row = $('#item-row-template');
        var cloneRow = row.clone();
        cloneRow.css({display: "block"});
        cloneRow.appendTo('#order-items');
        cloneRow.attr("id", "");
    };

    $('.plus-sign').on('click', function(e) {
        cloneRow();
        e.preventDefault();
    });

    setTimeout(function(){
        if ($("#order-items").children().length === 1)
            cloneRow();
    }, 0);

    $("#formSubmit").click(function(e)
    {
        e.preventDefault();
        parseForm();
        $("#orderForm").submit();
    })
});
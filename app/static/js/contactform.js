function parseForm() {
    var name = document.querySelector('#name').value;
    var email = document.querySelector('#email').value;
    var phone = document.querySelector('#phone').value;
    var message = document.querySelector('#message').value;

    var response = {
        "name" : name,
        "email" : email,
        "phone" : phone,
        "message" : message
    };

    var responseJSON = JSON.stringify(response);
    $('#formData').val(responseJSON);
}

$(function(){
    $('#formSubmit').click(function(){
        parseForm();
        $('#contact-form').submit();
    })
});
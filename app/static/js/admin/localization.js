$(document).ready(function() {
    $('.localization-clear-btn').on('click', function(e) {
        e.preventDefault();
        var flag = confirm("Jste si jisti že chcete vymazat lokalizaci?");
        if (flag)
        {
            location.href = "/admin/localization/clear"
        }
    })
});
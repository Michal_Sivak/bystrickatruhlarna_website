$(function(){
    $('.order-archive-btn').click(function(e){
        e.preventDefault();
        var flag = confirm("Jste si jistí že chcete provést tuto akci?");
        if (flag) { $.post('/admin/orders/archive', { "id" : $(this)[0].dataset.archive }, function() {location.reload();})};
    })
    $('.order-delete-btn').click(function(e){
        e.preventDefault();
        var flag = confirm("Jste si jistí že chcete provést tuto akci?");
        if (flag) { $.post('/admin/orders/delete', { "id" : $(this)[0].dataset.archive }, function() {location.reload();})};
    })
});
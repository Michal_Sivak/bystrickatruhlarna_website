function parseForm() {
    var name = document.querySelector('.offer-name').value;
    var from = document.querySelector('.from-date').value;
    var to = document.querySelector('.to-date').value;
    var text = document.querySelector('.offer-text').value;

    var response = {
        "name" : name,
        "from-date" : from,
        "to-date" : to,
        "offer-text" : text 
    }
    
    var responseJSON = JSON.stringify(response);
    $("#formData").val(responseJSON);
}

$(function(){
    $('#datetimepicker1').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-3d'
    });
    $('#datetimepicker2').datepicker({
        format: 'dd/mm/yyyy',
        startDate: '-3d'
    });

    $('#formSubmit').click(function(e){
        e.preventDefault();
        parseForm();
        $('#offerAddForm').submit();
    })
    $('.offer-archive-btn').click(function(e){
        e.preventDefault();
        var flag = confirm("Jste si jistí že chcete provést tuto akci?");
        if (flag) { $.post('/admin/offers/delete', { "id" : $(this)[0].dataset.archive }, function() {location.reload();} )};
    })
})
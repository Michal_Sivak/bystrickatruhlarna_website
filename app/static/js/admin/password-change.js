function parseForm() {
    var username = document.querySelector('#username').value;
    var oldPass = document.querySelector('#oldPasssword').value;
    var newPass = document.querySelector('#newPassword').value;

    if ($('#newPassword').val() == $('#newPasswordRepeat').val()) {
        var response = {
            "username" : username,
            "old_password" : oldPass,
            "new_password" : newPass
        }
        
        var responseJSON = JSON.stringify(response);
        $("#formData").val(responseJSON);
    }
}

$(function(){
    $('#formSubmit').click(function(e){
        e.preventDefault();
        parseForm();
        $('#passwordChangeForm').submit();
    })
    $('#newPassword, #newPasswordRepeat').on('keyup', function () {
        if ($('#newPassword').val() == $('#newPasswordRepeat').val()) {
            $('#newPassword').css('border-color', 'green');
            $('#newPasswordRepeat').css('border-color', 'green');
            $('#message').html('');
        }
        else 
        {
          $('#newPassword').css('border-color', '#a94442');
          $('#newPasswordRepeat').css('border-color', '#a94442');
          $('#message').html('Hesla se neshodují').css('color', 'red');
        }
    });
})
$(function(){
    tinymce.init({
        selector: '#czech',
        language: 'cs',
        menubar: false,
        toolbar: 'undo redo | bold italic underline strikethrough ',
        branding: false,
        force_br_newlines : false,
        force_p_newlines : false,
        forced_root_block : '',
        entity_encoding : "raw"
    });
    tinymce.init({
        selector: '#english',
        language: 'cs',
        menubar: false,
        toolbar: 'undo redo | bold italic underline strikethrough ',
        branding: false,
        force_br_newlines : false,
        force_p_newlines : false,
        forced_root_block : '',
        entity_encoding : "raw"
    });
    tinymce.init({
        selector: '#german',
        language: 'cs',
        menubar: false,
        toolbar: 'undo redo | bold italic underline strikethrough ',
        branding: false,
        force_br_newlines : false,
        force_p_newlines : false,
        forced_root_block : '',
        entity_encoding : "raw"
    });
})
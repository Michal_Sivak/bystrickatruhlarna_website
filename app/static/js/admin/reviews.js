function parseForm() {
    var name = document.querySelector('.review-name').value;
    var firm = document.querySelector('.review-firm').value;
    var text = document.querySelector('.review-text').value;

    var response = {
        "firm_name" : firm,
        "person_name" : name,
        "text" : text 
    }
    
    var responseJSON = JSON.stringify(response);
    $("#formData").val(responseJSON);
}

$(function(){
    $('.review-delete-btn').click(function(e){
        e.preventDefault();
        var flag = confirm("Jste si jistí že chcete provést tuto akci?");
        if (flag) { $.post('/admin/reviews/delete', { "id" : $(this)[0].dataset.archive }, function() {location.reload();})};
    })
    $('#formSubmit').click(function(e){
        e.preventDefault();
        parseForm();
        $('#reviewAddForm').submit();
    })
})
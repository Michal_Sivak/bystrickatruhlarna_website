import os

import polib

from app import db
from app.models import Languages

basedir = os.path.abspath(os.path.dirname(__file__))


def init(lang):
    os.system('pybabel extract -F babel.cfg -k _l -o messages.pot .')
    os.system('pybabel init -i messages.pot -d app/translations -l ' + lang)
    os.remove('messages.pot')


def save_keys_to_db():
    po = polib.pofile(basedir + '/translations/en/LC_MESSAGES/messages.po')
    for entry in po:
        db_entry = Languages(entry.msgid)
        if Languages.query.filter_by(key=entry.msgid).first() is None:
            db.session.add(db_entry)
            db.session.commit()


def read_languages_from_db(lang):
    po = polib.pofile(basedir + '/translations/' + lang + '/LC_MESSAGES/messages.po')
    for entry in po:
        sentence = Languages.query.filter_by(key=entry.msgid).first()
        translation = {
            "cs": sentence.czech,
            "en": sentence.english,
            "de": sentence.german
        }
        if translation[lang] is not None:
            entry.msgstr = translation[lang]
    po.save(basedir + '/translations/' + lang + '/LC_MESSAGES/messages.po')


def update():
    os.system('pybabel extract -F babel.cfg -k _l -o messages.pot .')
    os.system('pybabel update -i messages.pot -d app/translations')
    os.remove('messages.pot')


def compile():
    os.system('pybabel compile -d app/translations')

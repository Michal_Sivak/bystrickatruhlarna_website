from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, login


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


@login.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class Orders(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    recipient = db.Column(db.String(256))
    firm = db.Column(db.String(256))
    address = db.Column(db.String(1024))
    phone = db.Column(db.String(9))
    name = db.Column(db.String(256))
    archived = db.Column(db.Boolean(), default=False)
    items = db.relationship("Item", backref="orders", lazy=True)

    def __init__(self, recipient, firm, address, phone, name):
        self.recipient = recipient
        self.firm = firm
        self.address = address
        self.phone = phone
        self.name = name

    def __repr__(self):
        return "<Order from {}>".format(self.recipient)


class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    material = db.Column(db.String(256))
    length = db.Column(db.Integer)
    absolute_length = db.Column(db.Integer)
    width = db.Column(db.Integer)
    absolute_width = db.Column(db.Integer)
    count = db.Column(db.Integer)
    item_type = db.Column(db.String(256))
    item_id = db.Column(db.Integer, db.ForeignKey('orders.id'), nullable=False)
    item = db.relationship("Orders")

    def __init__(self, material, length, absolute_length, width, absolute_width, count, item_type):
        self.material = material
        self.length = int(length)
        self.absolute_length = int(absolute_length)
        self.width = int(width)
        self.absolute_width = int(absolute_width)
        self.count = int(count)
        self.item_type = item_type

    def __repr__(self):
        return "<Item {}>".format(self.item_id)


class Offers(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))
    von = db.Column(db.DateTime)
    zu = db.Column(db.DateTime)
    text = db.Column(db.String(16384))
    archived = db.Column(db.Boolean, default=False)

    def __init__(self, name, von, zu, text):
        self.name = name
        self.von = von
        self.zu = zu
        self.text = text

    def __repr__(self):
        return "<Offer {}>".format(self.name)


class Reviews(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firm_name = db.Column(db.String(256))
    person_name = db.Column(db.String(256))
    text = db.Column(db.String(2056))

    def __init__(self, firm_name, person_name, text):
        self.firm_name = firm_name
        self.person_name = person_name
        self.text = text

    def __repr__(self):
        return "<Review {}>".format(self.name)


class Languages(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String(4096))
    czech = db.Column(db.String(4096))
    english = db.Column(db.String(4096))
    german = db.Column(db.String(4096))

    def __init__(self, key):
        self.key = key

    def __repr__(self):
        return "<Sentence {}>".format(self.key)

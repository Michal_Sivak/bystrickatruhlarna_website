from app import create_app, db
from app.models import User, Orders, Reviews, Offers

app = create_app()


@app.before_first_request
def create_first_user():
    user = User()
    user.username = "admin"
    user.set_password("admin")
    if User.query.filter_by(username=user.username).first() is None:
        db.session.add(user)
        db.session.commit()


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Orders': Orders, 'Offers': Offers, 'Reviews': Reviews}
